package main

import (
	"fmt"
	"os"
)

func dumphex(a uint32) string {
	// 下位バイトを前に置く
	return fmt.Sprintf("%02x%02x%02x%02x", byte(a%256), byte((a>>8)%256), byte((a>>16)%256), byte((a>>24)%256))
}
func printhex(a uint32) {
	// デバッグ用
	fmt.Println(fmt.Sprintf("%02x%02x%02x%02x", byte((a>>24)%256), byte((a>>16)%256), byte((a>>8)%256), byte(a%256)))
}
func printbin(a uint32) {
	// デバッグ用
	var i uint32
	for i = 31; ; i-- {
		fmt.Print((a >> i) % 2)
		if i == 0 {
			break
		}
	}
	fmt.Println("")
}

func leftrotate(f uint32, s uint32) uint32 {
	return (f << s) | (f >> (32 - s))
}

func main() {
	// 参考:
	// https://ja.wikipedia.org/wiki/MD5
	// https://ryozi.hatenadiary.jp/entry/20100626/1277529116
	// https://www.ipa.go.jp/security/rfc/RFC1321JA.html

	/////////////////////////
	// 定数の宣言
	/////////////////////////
	K := [64]uint32{0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee, 0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501, 0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be, 0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821, 0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa, 0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8, 0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed, 0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a, 0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c, 0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70, 0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05, 0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665, 0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039, 0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1, 0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1, 0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391}
	S := [64]uint32{7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21}

	const A0INIT uint32 = 0x67452301
	const B0INIT uint32 = 0xefcdab89
	const C0INIT uint32 = 0x98badcfe
	const D0INIT uint32 = 0x10325476

	const BUFSIZE = 1024 * 1024

	/////////////////////////
	// 標準入力から入力データ読み取り
	////////////////////////
	buf := make([]byte, (BUFSIZE + 100)) // とりあえず入力は 1MB 未満である前提. 全部メモリに乗せる.
	bufSize, err := os.Stdin.Read(buf)
	if err != nil {
		panic(1)
	}
	if bufSize >= BUFSIZE {
		panic(1)
	}

	//////////////////////////
	// パディング
	/////////////////////////
	size := bufSize
	buf[size] = 0x80
	size++
	for {
		if (size % 64) == 56 {
			break
		} else {
			buf[size] = 0x00
			size++
		}
	}

	/////////////////////////
	// 入力データのサイズを末尾に追加   (2^64bitより長いものは考えない)
	/////////////////////////

	// 注意：長さの下位バイトから順に、bufの前方バイトから入れていく
	var length int = bufSize * 8
	buf[size] = byte(length % 256)
	buf[size+1] = byte((length >> 8) % 256)
	buf[size+2] = byte((length >> 16) % 256)
	buf[size+3] = byte((length >> 24) % 256)
	buf[size+4] = byte((length >> 32) % 256)
	buf[size+5] = byte((length >> 40) % 256)
	buf[size+6] = byte((length >> 48) % 256)
	buf[size+7] = byte((length >> 56) % 256)

	msgSize := int(size + 8)

	//////////////////
	// 繰り返し処理
	//////////////////
	A0 := A0INIT
	B0 := B0INIT
	C0 := C0INIT
	D0 := D0INIT

	msgIdx := 0
	var M [16]uint32

	for msgIdx < msgSize {
		for i := 0; i < 16; i++ {
			// 注意：bufの前方にあるバイトが、Mの後方に入る
			M[i] = (uint32(buf[msgIdx+3]) << 24) + (uint32(buf[msgIdx+2]) << 16) + (uint32(buf[msgIdx+1]) << 8) + uint32(buf[msgIdx+0])
			msgIdx += 4
		}
		A := A0
		B := B0
		C := C0
		D := D0

		var F uint32
		var g int
		for i := 0; i < 64; i++ {
			if i < 16 {
				F = (B & C) | ((^B) & D)
				g = i
			} else if i < 32 {
				F = (B & D) | (C & (^D))
				g = (5*i + 1) % 16
			} else if i < 48 {
				F = (B ^ C) ^ D
				g = (3*i + 5) % 16
			} else {
				F = C ^ (B | (^D))
				g = (7 * i) % 16
			}
			F = F + A + M[g] + K[i]
			A = D
			D = C
			C = B
			B = B + leftrotate(F, S[i])
		}
		A0 = A0 + A
		B0 = B0 + B
		C0 = C0 + C
		D0 = D0 + D
	}

	result := dumphex(A0) + dumphex(B0) + dumphex(C0) + dumphex(D0)
	fmt.Println(result)
}
